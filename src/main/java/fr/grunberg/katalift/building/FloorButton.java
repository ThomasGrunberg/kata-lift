package fr.grunberg.katalift.building;

import java.util.Optional;

import fr.grunberg.katalift.building.lift.Lift;

/**
 * An up or down floor button to call an elevator
 */
public class FloorButton {
	private final Floor floor;
	private final Direction direction;
	private boolean lit;

	public FloorButton(Floor floor, Direction direction) {
		this.floor = floor;
		this.direction = direction;
		lit = false;
	}
	
	public void press() {
		if(lit)
			return;
		Optional<Lift> calledLift = floor.getBuilding().getLifts().stream()
			.filter(lift -> lift.call(floor, direction))
			.findFirst();
		if(calledLift.isPresent())
			lit = true;
	}
	
	public void turnOff() {
		lit = false;
	}

	public boolean isLit() {
		return lit;
	}
	
	public Floor getFloor() {
		return floor;
	}

	public Direction getDirection() {
		return direction;
	}
}
