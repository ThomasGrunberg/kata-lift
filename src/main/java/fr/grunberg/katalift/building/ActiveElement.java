package fr.grunberg.katalift.building;

/**
 * An element that can move on its own
 */
public interface ActiveElement {
	public void act();
}
