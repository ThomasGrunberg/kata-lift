package fr.grunberg.katalift.building.lift;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.katalift.building.Building;
import fr.grunberg.katalift.building.Direction;
import fr.grunberg.katalift.building.Floor;

/**
 * A basic lift, answering the first call to action only
 */
public class BasicLift extends Lift {
	private Floor destination;
	private List<Floor> destinations = new ArrayList<>(1);

	public BasicLift(Building building, int id) {
		super(building, id);
	}


	protected void setDestination(Floor destination) {
		this.destination = destination;
		destinations.clear();
		if(destination != null)
			destinations.add(destination);
	}
	
	@Override
	public boolean call(Floor floor, Direction direction) {
		if(destination != null)
			return false;
		setDestination(floor);
		return true;
	}

	@Override
	public void pressDestinationButton(Floor destination) {
		if(this.destination != null)
			return;
		setDestination(destination);
	}

	public List<Floor> getDestinations() {
		return destinations;
	}

	@Override
	public Optional<Floor> getDestination() {
		if(destinations.size() > 0)
			return Optional.of(destinations.get(0));
		return Optional.empty();
	}
}
