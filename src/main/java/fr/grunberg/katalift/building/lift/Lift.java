package fr.grunberg.katalift.building.lift;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.katalift.KataLift;
import fr.grunberg.katalift.building.ActiveElement;
import fr.grunberg.katalift.building.Building;
import fr.grunberg.katalift.building.Direction;
import fr.grunberg.katalift.building.Floor;
import fr.grunberg.katalift.building.Person;
import fr.grunberg.katalift.building.Position;

/**
 * A lift/elevator, that can move from one floor to another, carry people, and respond to floor signals
 */
public abstract class Lift implements Position, ActiveElement {
	private final Building building;
	private final int id;
	private final List<Floor> servicedFloors = new ArrayList<>();
	private final List<Person> personsInLift = new ArrayList<>();
	private Floor currentFloor;
	private boolean doorsOpen;

	public Lift(Building building, int id) {
		this.building = building;
		currentFloor = building.getFloorLevel(1);
		this.id = id;
		setDoorsOpen(false);
	}
	
	public Building getBuilding() {
		return building;
	}
	
	public void serviceFloor(Floor floor) {
		servicedFloors.add(floor);
	}
	public void serviceFloors(List<Floor> floors) {
		servicedFloors.addAll(floors);
	}
	
	public boolean isFloorServiced(Floor floor) {
		return servicedFloors.contains(floor);
	}

	public abstract boolean call(Floor floor, Direction direction);
	
	public boolean stepIn(Person person) {
		personsInLift.add(person);
		person.setCurrentPosition(this);
		return true;
	}
	
	public boolean stepOut(Person person) {
		personsInLift.remove(person);
		person.setCurrentPosition(currentFloor);
		return true;
	}

	public Floor getCurrentFloor() {
		return currentFloor;
	}

	public boolean areDoorsOpen() {
		return doorsOpen;
	}

	public void setDoorsOpen(boolean doorsOpen) {
		this.doorsOpen = doorsOpen;
	}

	public abstract void pressDestinationButton(Floor destination);

	protected void move(Direction direction) {
		if(direction == Direction.UP)
			currentFloor = building.getFloorLevel(getCurrentFloor().getLevel() + 1);
		else
			currentFloor = building.getFloorLevel(getCurrentFloor().getLevel() - 1);
	}

	public int getId() {
		return id;
	}

	public String toString() {
		return "Lift-" + id;
	}
	
	public abstract List<Floor> getDestinations();
	
	@Override
	public void act() {
		if(!getDestination().isPresent())
			return;
		if(getDestination().get() == getCurrentFloor() && !areDoorsOpen()) {
			setDoorsOpen(true);
			getCurrentFloor().getButton(Direction.UP).turnOff();
			getCurrentFloor().getButton(Direction.DOWN).turnOff();
			if(KataLift.DebugFloorActions)
				System.out.println(this + " has arrived at floor " + getCurrentFloor().getLevel() + " and opened the doors.");
			setDestination(null);
			return;
		}
		if(getDestination().get() != getCurrentFloor() && areDoorsOpen()) {
			if(KataLift.DebugFloorActions)
				System.out.println(this + " closed its doors.");
			setDoorsOpen(false);
			return;
		}
		if(getDestination().get() != getCurrentFloor() && !areDoorsOpen()) {
			move(Building.getDirection(getCurrentFloor(), getDestination().get()));
			if(KataLift.DebugFloorActions)
				System.out.println(this + " moved to " + getCurrentFloor());
			return;
		}
	}

	public abstract Optional<Floor> getDestination();
	protected abstract void setDestination(Floor floor);
}
