package fr.grunberg.katalift.building;

import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.katalift.building.lift.Lift;

public class Floor implements Position {
	private final Building building;
	private final FloorButton buttonUp, buttonDown;
	private final int level;
	
	public Floor(Building building, int level) {
		this.building = building;
		this.level = level;
		buttonUp = new FloorButton(this, Direction.UP);
		buttonDown = new FloorButton(this, Direction.DOWN);
	}
	
	public FloorButton getButton(Direction direction) {
		if(direction == Direction.UP)
			return buttonUp;
		return buttonDown;
	}
	
	public Building getBuilding() {
		return building;
	}

	public Integer getLevel() {
		return level;
	}

	public List<Lift> getAllPresentLifts() {
		return building.getLifts().stream()
			.filter(lift -> lift.getCurrentFloor() == this)
			.collect(Collectors.toList());
	}
	
	public String toString() {
		return "Floor " + level;
	}
}
