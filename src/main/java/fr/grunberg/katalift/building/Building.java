package fr.grunberg.katalift.building;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.katalift.building.exception.UnknownFloorException;
import fr.grunberg.katalift.building.lift.BasicLift;
import fr.grunberg.katalift.building.lift.Lift;

public class Building {
	private final List<Floor> floors = new ArrayList<>();
	private final List<Lift> lifts = new ArrayList<>();
	private final List<Person> persons = new ArrayList<>();
	
	public void addFloors(int numberOfFloors) {
		for(int i = 0; i < numberOfFloors; i++)
			floors.add(new Floor(this, i+1));
	}

	public void addLift(int numberOfLifts) {
		for(int i = 0; i < numberOfLifts; i++) {
			Lift lift = new BasicLift(this, i+1);
			lifts.add(lift);
		}
	}

	/**
	 * Returns the floor corresponding to that level
	 * @param level
	 * @return
	 */
	public Floor getFloorLevel(int level) {
		Optional<Floor> targetFloor =  floors.stream()
			.filter(f -> f.getLevel() == level)
			.findFirst();
		if(targetFloor.isPresent())
			return targetFloor.get();
		throw new UnknownFloorException(this, level);
	}
	
	public List<Person> getPersons() {
		return persons;
	}
	
	public void addPerson(String name, int startingFloorLevel, int destinationFloorLevel) {
		Floor originFloor = getFloorLevel(startingFloorLevel);
		Floor destinationFloor = getFloorLevel(destinationFloorLevel);
		persons.add(new Person(name, originFloor, destinationFloor));
	}

	public List<Lift> getLifts() {
		return lifts;
	}
	
	public static Direction getDirection(Floor source, Floor target) {
		if(source.getLevel() > target.getLevel())
			return Direction.DOWN;
		return Direction.UP;
	}
}
