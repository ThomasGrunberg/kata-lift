package fr.grunberg.katalift.building;

import java.util.Optional;

import fr.grunberg.katalift.KataLift;
import fr.grunberg.katalift.building.lift.Lift;

/**
 * A person, willing to go from one floor to another
 */
public class Person implements ActiveElement {
	private final String name;
	private Position currentPosition;
	private final Floor destination;

	public Person(String name, Floor currentPosition, Floor destination) {
		this.currentPosition = currentPosition;
		this.destination = destination;
		this.name = name;
	}
	
	public Position getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(Position currentPosition) {
		this.currentPosition = currentPosition;
	}

	public Floor getDestination() {
		return destination;
	}

	public boolean hasArrived() {
		return (currentPosition == destination);
	}

	public Optional<Floor> getCurrentFloor() {
		if(currentPosition instanceof Floor)
			return Optional.of((Floor) currentPosition);
		return Optional.empty();
	}

	@Override
	public void act() {
		if(hasArrived())
			return;
		if(currentPosition instanceof Lift) {
			Lift currentLift = (Lift) currentPosition;
			if(currentLift.getCurrentFloor() == destination && currentLift.areDoorsOpen()) {
				currentLift.stepOut(this);
				if(KataLift.DebugPeopleActions)
					System.out.println(this + " has arrived at their destination : floor " + destination.getLevel());
				return;
			}
			if(currentLift.getCurrentFloor() == destination && !currentLift.areDoorsOpen())
				return;
			if(currentLift.getCurrentFloor() != destination) {
				currentLift.pressDestinationButton(destination);
				return;
			}
		}
		Floor currentFloor = (Floor) currentPosition;
		if(currentFloor.getButton(Building.getDirection(currentFloor, destination)).isLit()) {
			if(KataLift.DebugPeopleActions)
				System.out.println(this + " waiting, the button is pressed.");
			return;
		}
		Optional<Lift> availableLift = currentFloor.getAllPresentLifts()
				.stream().filter(lift -> lift.areDoorsOpen() && lift.getDestinations().size() == 0)
				.findFirst();
		if(availableLift.isPresent()) {
			availableLift.get().stepIn(this);
			availableLift.get().pressDestinationButton(getDestination());
			if(KataLift.DebugPeopleActions)
				System.out.println(this + " enters " + availableLift.get() + " and presses the button for " + getDestination());
			return;
		}
		currentFloor.getButton(Building.getDirection(currentFloor, destination)).press();
		if(KataLift.DebugPeopleActions)
			System.out.println(this + " presses the button.");

	}
	
	public String toString() {
		return name;
	}
}
