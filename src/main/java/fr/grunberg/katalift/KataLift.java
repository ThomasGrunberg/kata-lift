package fr.grunberg.katalift;

import java.util.List;

import fr.grunberg.katalift.building.Building;
import fr.grunberg.katalift.building.Person;
import fr.grunberg.katalift.building.lift.Lift;

public class KataLift {
public static final boolean 
 DebugBuildingActions = true
,DebugFloorActions = true
,DebugPeopleActions = true
;
	
	public static void simulateTime(int timeStepsToSimulate, Building testBuilding) {
		try {
			if(DebugBuildingActions)
				System.out.println("---");
			List<Person> persons = testBuilding.getPersons();
			List<Lift> lifts = testBuilding.getLifts();
			
			for(int t = 0; t < timeStepsToSimulate; t++) {
				persons.stream()
					.forEach(person -> person.act());
	
				lifts.stream()
					.forEach(lift -> lift.act());
			}
			if(DebugBuildingActions)
				System.out.println("---");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
}
