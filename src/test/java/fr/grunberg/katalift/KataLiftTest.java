package fr.grunberg.katalift;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.grunberg.katalift.building.Building;
import fr.grunberg.katalift.building.Person;

/**
 * Tests JUnit 
 * @author Thomas Grunberg
 *
 */
public class KataLiftTest {
	@Test
	public void OnePersonOneLiftTwoFloors() {
		Building testBuilding = new Building();
		testBuilding.addFloors(2);
		testBuilding.addLift(1);
		
		testBuilding.addPerson("Peter", 1, 2);
		
		KataLift.simulateTime(50, testBuilding);
		List<Person> persons = testBuilding.getPersons();
		assertEquals(1, persons.size());

		assertTrue(persons.get(0).hasArrived());
		assertTrue(persons.get(0).getCurrentFloor().isPresent());
		assertEquals(2, persons.get(0).getCurrentFloor().get().getLevel());
	}

	@Test
	public void OnePersonOneLiftThreeFloors() {
		Building testBuilding = new Building();
		testBuilding.addFloors(3);
		testBuilding.addLift(1);
		
		testBuilding.addPerson("Jack", 1, 3);
		
		KataLift.simulateTime(50, testBuilding);
		List<Person> persons = testBuilding.getPersons();
		assertEquals(1, persons.size());

		assertTrue(persons.get(0).hasArrived());
		assertTrue(persons.get(0).getCurrentFloor().isPresent());
		assertEquals(3, persons.get(0).getCurrentFloor().get().getLevel());
	}

	@Test
	public void OnePersonTwoLiftsTwoFloors() {
		Building testBuilding = new Building();
		testBuilding.addFloors(2);
		testBuilding.addLift(2);
		
		testBuilding.addPerson("Patrick", 1, 2);
		
		KataLift.simulateTime(50, testBuilding);
		List<Person> persons = testBuilding.getPersons();
		assertEquals(1, persons.size());

		assertTrue(persons.get(0).hasArrived());
		assertTrue(persons.get(0).getCurrentFloor().isPresent());
		assertEquals(2, persons.get(0).getCurrentFloor().get().getLevel());
	}

	@Test
	public void TwoPersonsTwoLiftsThreeFloors() {
		Building testBuilding = new Building();
		testBuilding.addFloors(3);
		testBuilding.addLift(2);
		
		testBuilding.addPerson("Vanessa", 1, 2);
		testBuilding.addPerson("Judith", 1, 3);
		
		KataLift.simulateTime(50, testBuilding);
		List<Person> persons = testBuilding.getPersons();
		assertEquals(2, persons.size());

		assertTrue(persons.get(0).hasArrived());
		assertTrue(persons.get(0).getCurrentFloor().isPresent());
		assertEquals(2, persons.get(0).getCurrentFloor().get().getLevel());

		assertTrue(persons.get(1).hasArrived());
		assertTrue(persons.get(1).getCurrentFloor().isPresent());
		assertEquals(3, persons.get(1).getCurrentFloor().get().getLevel());
	}
}
